package com.onethesis.accel.constant;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;

import java.util.List;
import java.util.UUID;

public class GattProfile {
//SERVICE: 1: 00001800-0000-1000-8000-00805f9b34fb
//   Charac: 3: 00002a00-0000-1000-8000-00805f9b34fb
//   Charac: 5: 00002a01-0000-1000-8000-00805f9b34fb
//   Charac: 7: 00002a04-0000-1000-8000-00805f9b34fb
//   Charac: 9: 00002aa6-0000-1000-8000-00805f9b34fb
//   Charac: 11: 00002ac9-0000-1000-8000-00805f9b34fb
//SERVICE: 12: 00001801-0000-1000-8000-00805f9b34fb
//   Charac: 14: 00002a05-0000-1000-8000-00805f9b34fb
//   descriptors: 00002902-0000-1000-8000-00805f9b34fb
//SERVICE: 16: 0000180a-0000-1000-8000-00805f9b34fb
//   Charac: 18: 00002a29-0000-1000-8000-00805f9b34fb
//   Charac: 20: 00002a24-0000-1000-8000-00805f9b34fb
//   Charac: 22: 00002a27-0000-1000-8000-00805f9b34fb
//   Charac: 24: 00002a26-0000-1000-8000-00805f9b34fb
//SERVICE: 25: 89248e86-48d1-43df-b25c-5c0e3dfb858f
//   Charac: 27: f81e56d4-54d5-4dd4-be72-8291a336f21e
//   descriptors: 00002902-0000-1000-8000-00805f9b34fb

	public static UUID SERVICE_DATA =
			UUID.fromString("89248e86-48d1-43df-b25c-5c0e3dfb858f");
	public static UUID CHARACTERISTIC_SENSORS =
			UUID.fromString("f81e56d4-54d5-4dd4-be72-8291a336f21e");
	public static UUID DESCRIPTOR_SENSORS_CCCD =
			UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

	public static void checkProfile(BluetoothGatt gatt) {
		List<BluetoothGattService> services = gatt.getServices();
		for (BluetoothGattService service: services) {
			System.out.println("SERVICE: " + service.getInstanceId() +": " + service.getUuid().toString());
			List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
			for (BluetoothGattCharacteristic characteristic: characteristics) {
				System.out.println("   Charac: " + characteristic.getInstanceId() +": " + characteristic.getUuid().toString());
				List<BluetoothGattDescriptor> descriptors = characteristic.getDescriptors();
				if (descriptors.size() == 0) {
					continue;
				}
				for (BluetoothGattDescriptor descriptor: descriptors) {
					System.out.println("   descriptors: " + descriptor.getUuid().toString());
				}
			}
		}
	}
}

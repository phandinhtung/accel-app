package com.onethesis.accel.ble;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;

import com.onethesis.accel.constant.GattProfile;
import com.onethesis.accel.model.BleData;

import java.util.Arrays;
import java.util.UUID;

public class GattController extends BluetoothGattCallback {
	public static final int STATE_DISCOVERY = 4;
	public static final int STATE_DISCOVERY_FAIL = 5;

	private BleHandle bleHandle;

	public GattController(BleHandle bleHandle) {
		this.bleHandle = bleHandle;
	}

	// region Over function
	@Override
	public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
		bleHandle.onNewState(newState);
		if (newState == BluetoothProfile.STATE_CONNECTED) {
			gatt.discoverServices();
		}
	}

	@Override
	public void onServicesDiscovered(BluetoothGatt gatt, int status) {
		if (status != BluetoothGatt.GATT_SUCCESS) {
			bleHandle.onNewState(STATE_DISCOVERY_FAIL);
			gatt.discoverServices();
			return;
		}
		bleHandle.onNewState(STATE_DISCOVERY);
		GattProfile.checkProfile(gatt);
		sendData(gatt);
	}

	@Override
	public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
		if (status != BluetoothGatt.GATT_SUCCESS) {
			return;
		}
	}

	@Override
	public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
		if (status != BluetoothGatt.GATT_SUCCESS) {
			return;
		}
	}

	@Override
	public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
		if (!BleData.validateBleResponse(characteristic.getValue())) {
			System.out.println("GattController.onCharacteristicChanged data is in wrong format");
			return;
		}
		BleData bleData = new BleData(characteristic.getValue());
		System.out.println("BLE data: " + Arrays.toString(characteristic.getValue()));
		bleHandle.onNewResponse(bleData);
	}

	@Override
	public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
		if (status != BluetoothGatt.GATT_SUCCESS) {
			return;
		}
		System.out.println("GattController.onDescriptorWrite OK");
	}

	@Override
	public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
		super.onMtuChanged(gatt, mtu, status);

		if (status == BluetoothGatt.GATT_SUCCESS) {
			System.out.println("GattController.onMtuChanged " + mtu);
			sendNotifyDescriptorSensor(gatt);
		}
	}
	// endregion

	// region Read init data
	private void sendData(BluetoothGatt gatt) {
		gatt.requestMtu(512);
	}

	private void sendNotifyDescriptorSensor(BluetoothGatt gatt) {
		sendDescriptorCharacteristic(gatt, GattProfile.SERVICE_DATA,
				GattProfile.CHARACTERISTIC_SENSORS,
				GattProfile.DESCRIPTOR_SENSORS_CCCD,
				true);
	}
	// endregion

	// region Send characteristic utility
	private void sendReadCharacteristic(BluetoothGatt gatt, UUID serviceID, UUID characterID) {
		BluetoothGattService service = gatt.getService(serviceID);
		if (service != null) {
			BluetoothGattCharacteristic characteristic = service.getCharacteristic(characterID);
			if (characteristic != null) {
				gatt.readCharacteristic(characteristic);
			}
		}
	}

	private void sendWriteCharacteristic(BluetoothGatt gatt, UUID serviceID, UUID characterID, byte[] data) {
		BluetoothGattService service = gatt.getService(serviceID);
		if (service != null) {
			BluetoothGattCharacteristic characteristic = service.getCharacteristic(characterID);
			if (characteristic != null) {
				characteristic.setValue(data);
				System.out.println("GattController WriteCha: " + Arrays.toString(data));
				gatt.writeCharacteristic(characteristic);
			}
		}
	}

	private void sendDescriptorCharacteristic(BluetoothGatt gatt, UUID serviceID, UUID characterID, UUID descriptorID, boolean enable) {
		BluetoothGattService service = gatt.getService(serviceID);
		if (service == null) {
			System.out.println("Service not found");
			return;
		}
		BluetoothGattCharacteristic characteristic = service.getCharacteristic(characterID);
		if (characteristic == null) {
			System.out.println("characteristic not found");
			return;
		}


		gatt.setCharacteristicNotification(characteristic, enable);
		BluetoothGattDescriptor descriptor = characteristic.getDescriptor(descriptorID);

		if (descriptor != null) {
			descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
			gatt.writeDescriptor(descriptor);
		}
	}
	// endregion
}

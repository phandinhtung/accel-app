package com.onethesis.accel.ui.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.onethesis.accel.model.BLEDevice;
import com.onethesis.accel.ui.adapter.DeviceListAdapter;
import com.onethesis.accel.R;

import java.util.ArrayList;
import java.util.Collections;


public class ScanDeviceActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {
	// region Define
	public static final int REQUEST_ACCESS_COARSE_LOCATION = 1;
	public static final int REQUEST_ACCESS_GPS_LOCATION = 111;
	public static final int REQUEST_ENABLE_BLUETOOTH = 11;
	public static final int REQUEST_ACCESS_STORAGE = 112;

	private BluetoothAdapter bluetoothAdapter;
	private boolean isScanning = false;
	private final ArrayList<BLEDevice> listDevice = new ArrayList<>();
	private DeviceListAdapter deviceListAdapter;
	private final Runnable runnable = () -> {
		Toast.makeText(this, "Scan complete", Toast.LENGTH_SHORT).show();
	};
	private final Handler handler = new Handler();
	private SwipeRefreshLayout refreshLayout;
	// endregion

	// region activity override function
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_device);

		refreshLayout = findViewById(R.id.swipe_view);
		refreshLayout.setOnRefreshListener(this);

		ListView lvDevice = findViewById(R.id.lv_device);
		deviceListAdapter = new DeviceListAdapter(this, listDevice);
		lvDevice.setAdapter(deviceListAdapter);
		lvDevice.setOnItemClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		checkPermission();
	}
	// endregion

	// region Check permission
	private void checkPermission() {
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (bluetoothAdapter == null) {
			Toast.makeText(this, "Device not support Bluetooth", Toast.LENGTH_SHORT).show();
			return;
		}

		if (!bluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH);
			return;
		}

		checkCoarseLocationPermission();
	}

	private void checkCoarseLocationPermission() {
		int hasPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);

		if (hasPermission == PackageManager.PERMISSION_GRANTED) {
			checkGPSEnable();
			return;
		}
		requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
				REQUEST_ACCESS_COARSE_LOCATION);
	}

	private void checkGPSEnable() {
		LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		if (manager == null) return;
		if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			Intent intentOpenGPS =
					new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivityForResult(intentOpenGPS, REQUEST_ACCESS_GPS_LOCATION);
			return;
		}
		startScan();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
			if (!bluetoothAdapter.isEnabled()) {
				return;
			}

			checkCoarseLocationPermission();
			return;
		}

		if (requestCode == REQUEST_ACCESS_GPS_LOCATION) {
			LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
			if (manager == null) return;
			if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				return;
			}

			checkPermission();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == REQUEST_ACCESS_COARSE_LOCATION) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				checkGPSEnable();
				return;
			}

			checkPermission();
		}
		if (requestCode == REQUEST_ACCESS_STORAGE) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				return;
			}

			runOnUiThread(() -> {
				Toast.makeText(ScanDeviceActivity.this, "Please enable permission to access storage first", Toast.LENGTH_LONG).show();
			});
		}
	}
	// endregion

	// region Check storage permission
	private boolean checkStoragePermit() {
		if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
				== PackageManager.PERMISSION_GRANTED) {
			return true;
		} else {
			requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
					REQUEST_ACCESS_STORAGE);
			return false;
		}
	}
	// endregion

	// region Scan BLE
	private void startScan() {
		Toast.makeText(this, "Scanning", Toast.LENGTH_SHORT).show();

		isScanning = true;

		listDevice.clear();
		deviceListAdapter.notifyDataSetChanged();
		bluetoothAdapter.startLeScan(leScanCallback);
		handler.postDelayed(runnable, 10000);
	}

	private void stopScan() {
		if (isScanning) {
			isScanning = false;
			bluetoothAdapter.stopLeScan(leScanCallback);
		}
		handler.removeCallbacks(runnable);
	}

	@Override
	public void onRefresh() {
		refreshLayout.setRefreshing(false);
		stopScan();
		startScan();
	}

	BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi, byte[] scanRecord) {
			if (rssi >= -100 && checkDeviceExist(device)) {
				listDevice.add(new BLEDevice(device, rssi));
				Collections.sort(listDevice);
				deviceListAdapter.notifyDataSetChanged();
			}
		}

		private boolean checkDeviceExist(BluetoothDevice device) {
			for (int i = 0; i <listDevice.size(); i++) {
				if (listDevice.get(i).getDevice().getAddress().equals(device.getAddress())) {
					return false;
				}
			}

			return true;
		}
	};
	// endregion

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		stopScan();
		if (!checkStoragePermit()) {
			return;
		}
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra("address", listDevice.get(position).getDevice().getAddress());
		startActivity(intent);
		finish();
	}
}
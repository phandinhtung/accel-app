package com.onethesis.accel.model;

import com.onethesis.accel.util.DataParser;

import java.util.Arrays;

public class BleData {
	public long timestamp;
	public int battery;
	public int[] ADCValue = new int[251];

	public BleData(byte[] response) {
		timestamp = DataParser.parseUnsignedInt32(response, 0);
		battery = DataParser.parseUnsignedInt8(response[8]);
		for (int i = 0; i < 251; i++) {
			ADCValue[i] = DataParser.parseUnsignedInt16(response[i*2 + 5], response[i*2 + 6]);
		}
	}

	public static boolean validateBleResponse(byte[] response) {
		return response.length == 509;
	}

	@Override
	public String toString() {
		return "BleData{" +
				"timestamp=" + timestamp +
				", battery=" + battery +
				", ADCValue=" + Arrays.toString(ADCValue) +
				'}';
	}

	public String[] toLogString() {
		StringBuilder stringLog = new StringBuilder();
		stringLog.append(timestamp);
		stringLog.append(",").append(battery);
		for (int item: ADCValue) {
			stringLog.append(",").append(item);
		}

		return new String[] {stringLog.toString()};
	}
}

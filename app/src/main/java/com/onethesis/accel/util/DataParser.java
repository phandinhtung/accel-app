package com.onethesis.accel.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class DataParser {
	public static int parseUnsignedInt16(byte byte0, byte byte1) {
		int valueOfByte0 = parseUnsignedInt8(byte0);
		int valueOfByte1 = parseUnsignedInt8(byte1) << 8;
		return valueOfByte1 + valueOfByte0;
	}
	public static long parseUnsignedInt32(byte[] bytes, int srcPos) {
		long valueOfByte0 = parseUnsignedInt8(bytes[srcPos]);
		long valueOfByte1 = parseUnsignedInt8(bytes[srcPos + 1]) << 8;
		long valueOfByte2 = parseUnsignedInt8(bytes[srcPos + 2]) << 16;
		long valueOfByte3 = parseUnsignedInt8(bytes[srcPos + 3]) << 24;
		return valueOfByte3 + valueOfByte2 + valueOfByte1 + valueOfByte0;
	}
	public static long parseUnsignedInt64(byte[]bytes, int srcPos) {
		byte[] response = new byte[8];
		System.arraycopy(bytes, srcPos, response, 0, response.length);
		return ByteBuffer.wrap(response).order(ByteOrder.LITTLE_ENDIAN).getLong();
	}

	public static int parseUnsignedInt8(byte byte0) {
		return byte0 < 0 ? (byte0 + 256) : byte0;
	}
}

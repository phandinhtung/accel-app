package com.onethesis.accel.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.onethesis.accel.model.BLEDevice;
import com.onethesis.accel.R;

import java.util.List;

public class DeviceListAdapter extends ArrayAdapter<BLEDevice> {
	private final Context context;
	private final int resource;
	private final List<BLEDevice> listBleStores;
	public DeviceListAdapter(Context context, List<BLEDevice> objects) {
		super(context, R.layout.adapter_device, objects);
		this.context = context;
		this.resource = R.layout.adapter_device;
		this.listBleStores = objects;
	}

	public static class ViewHolder {
		TextView deviceName, deviceIp, deviceRSSI;
		View deviceView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;

		if (convertView == null) {
			convertView = LayoutInflater.from(this.context).inflate(this.resource, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.deviceName = convertView.findViewById(R.id.txt_device_name);
			viewHolder.deviceIp   = convertView.findViewById(R.id.txt_device_ip);
			viewHolder.deviceRSSI = convertView.findViewById(R.id.txt_device_rssi);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		BLEDevice device = listBleStores.get(position);
		viewHolder.deviceName.setText(device.getName());
		viewHolder.deviceIp.setText(device.getDevice().getAddress());
		viewHolder.deviceRSSI.setText((device.getRssi() + ""));

		return convertView;
	}
}

package com.onethesis.accel.ble;

import com.onethesis.accel.model.BleData;

public interface BleHandle {
	void onNewState(int state);
	void onNewResponse(BleData response);
}

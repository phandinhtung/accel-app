package com.onethesis.accel.util;

import androidx.annotation.NonNull;

import com.onethesis.accel.model.BLEDevice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;

public class AccelLog {
	public static final String NEW_LINE =  System.getProperty("line.separator");

	private static AccelLog instance;
	public static AccelLog getInstance() {
		return instance;
	}

	public static void clear() {
		instance = null;
	}

	public static void initLog(File mainDirectory, boolean clear) {
		instance = new AccelLog(mainDirectory, clear);
	}

	public File logFile;

	public AccelLog(File mainDirectory, boolean clear) {
		try {
			File logDirectory = new File(mainDirectory, "log");
			if (!logDirectory.exists()) {
				logDirectory.mkdir();
			} else {
				if (clear) {
					clearLogDirectory(logDirectory);
				}
			}
			String fileName = "Accel_" + Calendar.getInstance().getTimeInMillis() + ".csv";
			logFile = new File(logDirectory, fileName);
			System.out.println("LogManager.init " + logFile.getAbsolutePath());
			if (logFile.exists()) {
				logFile.delete();
			}
			logFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			logFile = null;
		}
	}

	private void clearLogDirectory(File logDirectory) {
		File[] files = logDirectory.listFiles();
		if (files == null) return;

		for (File file : files) {
			if (file != null && !file.isDirectory()) {
				file.delete();
			}
		}
	}

	public void logDevice(@NonNull BLEDevice bleDevice) {
		log(bleDevice.getName(), bleDevice.getDevice().getAddress());
		logHeader();
	}
	public void logHeader() {
		String[] strings = new String[253];
		strings[0] = "timestamp";
		strings[1] = "battery";
		for (int i = 2; i < 253; i++) {
			strings[i] = "data " + i;
		}
		log(strings);
	}

	void log(String text) {
		try {
			if (!logFile.exists()) {
				logFile.createNewFile();
			}
			FileOutputStream fileOutputStream = new FileOutputStream(logFile, true);

			OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);
			writer.append(text)
					.append(NEW_LINE);
			writer.close();
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void log(String... texts) {
		String stringLog = String.join(",", texts);
		log(stringLog);
	}
}

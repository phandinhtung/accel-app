package com.onethesis.accel.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.onethesis.accel.ble.BleHandle;
import com.onethesis.accel.ble.GattController;
import com.onethesis.accel.R;
import com.onethesis.accel.model.BLEDevice;
import com.onethesis.accel.model.BleData;
import com.onethesis.accel.util.AccelLog;

import java.io.File;
import java.net.URLConnection;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements BleHandle, View.OnClickListener {
	private TextView tvState, tvTimestamp, tvBattery, tvData;
	private BLEDevice bleDevice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Intent intent = getIntent();
		String bleAddress = intent.getStringExtra("address");
		connectBleDevice(bleAddress);
		tvState = findViewById(R.id.tv_state);
		tvTimestamp = findViewById(R.id.tv_timestamp);
		tvBattery = findViewById(R.id.tv_battery);
		tvData = findViewById(R.id.tv_axis);
		findViewById(R.id.btn_export).setOnClickListener(this);
	}

	private void connectBleDevice(String bleAddress) {
		GattController gattCallback = new GattController(this);

		BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(bleAddress);
		device.connectGatt(this, true, gattCallback);
		AccelLog.initLog(getFilesDir(), true);
		bleDevice = new BLEDevice(device, 0);
		AccelLog.getInstance().logDevice(bleDevice);
	}

	@Override
	public void onNewState(int state) {
		runOnUiThread(() -> {
			if (state == BluetoothProfile.STATE_DISCONNECTED) {
				tvState.setText(("Disconnected"));
				return;
			}
			if (state == BluetoothProfile.STATE_CONNECTED) {
				tvState.setText(("Discovering"));
				return;
			}
			if (state == BluetoothProfile.STATE_CONNECTING) {
				tvState.setText(("Connecting"));
				return;
			}
			if (state == BluetoothProfile.STATE_DISCONNECTING) {
				tvState.setText(("Disconnecting"));
				return;
			}
			if (state == GattController.STATE_DISCOVERY) {
				tvState.setText(("Connected"));
				return;
			}
			if (state == GattController.STATE_DISCOVERY_FAIL) {
				tvState.setText(("Discovering again"));
			}
		});
	}

	@Override
	public void onNewResponse(BleData response) {
		runOnUiThread(() -> {
			tvTimestamp.setText(("Timestamp: " + response.timestamp));
			tvBattery.setText(("Battery: " + response.battery));
			tvData.setText(("Data: " + Arrays.toString(response.ADCValue)));
		});
		AccelLog.getInstance().log(response.toLogString());
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_export) {
			File logFile = AccelLog.getInstance().logFile;
			AccelLog.clear();
			AccelLog.initLog(getFilesDir(), false);
			AccelLog.getInstance().logDevice(bleDevice);
			exportFile(logFile);
		}
	}

	private void exportFile(File logFile) {
		System.out.println("MainActivity.exportFile");
		Intent intentShareFile = new Intent(Intent.ACTION_SEND);
		if (logFile.exists()) {
			intentShareFile.setType(URLConnection.guessContentTypeFromName(logFile.getName()));

			Uri uri = FileProvider.getUriForFile(this,
					getString(R.string.file_provider_authority), logFile);

			intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);

			intentShareFile.putExtra(Intent.EXTRA_SUBJECT, logFile.getName());
			intentShareFile.putExtra(Intent.EXTRA_TEXT, logFile.getName());
			intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			try {
				startActivity(intentShareFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("MainActivity.exportFile failed");
		}
	}
}
package com.onethesis.accel.model;

import android.bluetooth.BluetoothDevice;

public class BLEDevice implements Comparable<BLEDevice> {
	private final BluetoothDevice device;
	private final int rssi;

	public BLEDevice(BluetoothDevice device, int rssi) {
		this.device = device;
		this.rssi = rssi;
	}

	public BluetoothDevice getDevice() {
		return device;
	}

	public int getRssi() {
		return rssi;
	}

	public String getName() {
		return (device.getName() == null || device.getName().equals("")) ? "Unknown" : this.device.getName();
	}

	@Override
	public int compareTo(BLEDevice bleStore) {
		return Integer.compare(bleStore.getRssi(), this.getRssi());
	}
}
